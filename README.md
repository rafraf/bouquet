# How to run
## Build
1. `docker build -t bloomon_rr_ch .`

## Run
2. `docker run -i --rm bloomon_rr_ch:latest python run.py`
3. Start manually inputting values according to the scheme from the requirement

#### * Alternatively, you could use the sample file from the task:
2. `cat sample.txt | docker run -i --rm bloomon_rr_ch:latest python run.py`

## Run tests
2. `docker run -i --rm bloomon_rr_ch:latest python -m unittest discover`


# Remove Docker images associated with 'bloomon_rr_ch' container
1. `docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'bloomon_rr_ch')`

# Didn't have time, but would have improved
* Splitting BouquetProducer and Parser to smaller pieces and more meaningful parts
* Make less prone to errors in input (ignoring wrong formatted flowers), controlling flow more with 'try except'
* Rewriting run.py, use Strategy or something similar instead of 'if else'
* More and better tests