FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /app

ADD . /app