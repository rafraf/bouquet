#!/usr/bin/env python3
import sys

from bloomon.parser import InputStreamer
from bloomon.producer import BouquetProducer
from bloomon.settings import input_file, source

lp = InputStreamer(BouquetProducer())

if source == 'input':
    for line in sys.stdin:
        lp.process(line=line)
else:
    with open(input_file) as f:
        for line in f:
            lp.process(line=line)
