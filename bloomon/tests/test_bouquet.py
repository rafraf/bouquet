import unittest
from bloomon.bouquet import Flower, Bouquet


class TestFlower(unittest.TestCase):
    def test_create(self):
        flower = Flower('s', 'L')
        self.assertEqual(type(flower), Flower)
        self.assertEqual(flower.size, 'L')


class TestBouquet(unittest.TestCase):
    def test_create(self):
        bq = Bouquet('A', 'L', [], 0)
        self.assertEqual(type(bq), Bouquet)
        self.assertEqual(bq.size, 'L')

