import unittest
from bloomon.bouquet import Bouquet, Flower
from bloomon.producer import BouquetProducer


class TestBouquetProducer(unittest.TestCase):
    def setUp(self) -> None:
        self.bq_p = BouquetProducer()
        self.bq = Bouquet('A', 'S', [(1, 'a'), (1, 'b'), (1, 'k')], 4)

    def test_add(self):
        self.assertEqual(len(self.bq_p.get_designs('S')), 0)
        self.bq_p.add_design(self.bq)
        self.assertEqual(len(self.bq_p.get_designs('S')), 1)

    def test_remove_fl_used_in_bouquet(self):
        bqp1 = BouquetProducer()
        bqp1.add_design(Bouquet('A', 'S', [(1, 'a'), (1, 'b'), (1, 'k')], 3))
        bqp1.add_design(Bouquet('B', 'S', [(2, 'a'), (3, 'b'), (4, 'k')], 9))
        bqp1.compile_bouquet(Flower('k', 'S'))
        bqp1.compile_bouquet(Flower('k', 'S'))
        bqp1.compile_bouquet(Flower('k', 'S'))
        bqp1.compile_bouquet(Flower('b', 'S'))
        bq = bqp1.compile_bouquet(Flower('a', 'S'))
        # self.assertEqual(type(bq), Bouquet)
        # self.assertEqual(str(bq), 'AS1a1b1k')
        for bq_d in bqp1.bouquet_designs:
            flower_a_count = len(bq_d.storage['a'])
            flower_b_count = len(bq_d.storage['b'])
            flower_k_count = len(bq_d.storage['k'])
            self.assertEqual(flower_a_count, 0)
            self.assertEqual(flower_b_count, 0)
            self.assertEqual(flower_k_count, 2)
