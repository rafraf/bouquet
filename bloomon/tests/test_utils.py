import unittest
from bloomon.bouquet import Flower, Bouquet
from bloomon.utils import create_bq_from_str, create_fl_from_str, adjust_size


class TestCreateBouquetFromString(unittest.TestCase):
    def test_create_bq_from_str_wrong(self):
        bq = create_bq_from_str('ssd')
        self.assertEqual(bq, None)

    def test_create_bq_from_str_valid(self):
        bq = create_bq_from_str('AS3a4b6k20')
        self.assertEqual(type(bq), Bouquet)


class TestCreateFlowerFromString(unittest.TestCase):
    def test_create_fl_from_str_wrong(self):
        fl = create_fl_from_str('123')
        self.assertEqual(fl, None)

    def test_create_fl_from_str_valid(self):
        fl = create_fl_from_str('aS')
        self.assertEqual(type(fl), Flower)


class TestAdjustSize(unittest.TestCase):
    def setUp(self) -> None:
        bq = create_bq_from_str('AS1a1b1k3')
        bq.add(create_fl_from_str('aS'))
        bq.add(create_fl_from_str('bS'))
        bq.add(create_fl_from_str('kS'))
        self.bq = bq

    def test_adjust_size_wrong(self):
        bq = self.bq
        new_bq = adjust_size(bq, 0)
        self.assertEqual(len(bq.flowers), 3)
        self.assertEqual(len(new_bq.flowers), 3)

        new_bq.add(create_fl_from_str('kS'))
        self.assertEqual(len(bq.flowers), 3)
        new_bq = adjust_size(bq, 1)
        self.assertEqual(len(new_bq.flowers), 3)

    def test_adjust_size_valid(self):
        bq = self.bq
        new_bq = adjust_size(bq, 0)
        self.assertEqual(len(new_bq.flowers), 3)

