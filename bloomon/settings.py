import re

bq_design_regex = re.compile(r'(?P<name>[A-Z])(?P<size>[A-Z])(?P<flowers>(\d+[a-z])+)(?P<total>\d+)')
bq_flowers_regex = re.compile(r'(\d+)([a-z])')
flowers_re = re.compile(r'([a-z])([A-Z])')

source = 'input'
input_file = 'sample.txt'
