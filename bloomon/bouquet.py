class Flower:
    species = None
    size = 0

    def __init__(self, species: str, size: str) -> None:
        self.species = species
        self.size = size


class Bouquet:
    name = ''
    size = 0
    flowers = {}
    max_number = 0
    storage = {}

    def __init__(self, name: str, size: str, flowers: list, max_number: int) -> None:
        self.name = name
        self.size = size
        self.flowers = {f[1]: int(f[0]) for f in flowers}
        self.max_number = max_number
        self.storage = {}

    def add(self, flower: Flower):
        """Add flower to bouquet"""
        if flower.species not in self.storage:
            self.storage[flower.species] = []
        self.storage[flower.species].append(flower)

    def complete(self) -> bool:
        """Checks whether bouquet is complete"""
        for f in self.flowers:
            if f not in self.storage:
                return False
            if self.flowers[f] > len(self.storage[f]):
                return False
        return True

    def __str__(self) -> str:
        """Example: AS10a4b6k"""
        sorted_flowers = []
        for f in sorted(self.storage):
            sorted_flowers.append(str(len(self.storage[f])) + f)
        return self.name + str(self.size) + ''.join([f for f in sorted_flowers])
