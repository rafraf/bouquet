import re
from typing import Union

from bloomon.bouquet import Bouquet, Flower
from bloomon.settings import bq_design_regex, bq_flowers_regex, flowers_re


def create_bq_from_str(string: str) -> Union[Bouquet, None]:
    bq_match = re.search(bq_design_regex, string)
    try:
        flowers_match = re.findall(bq_flowers_regex, bq_match.group('flowers'))
        name = bq_match.group('name')
        size = bq_match.group('size')
        flowers = flowers_match
        total = int(bq_match.group('total'))
    except AttributeError:
        return None
    return Bouquet(name=name, size=size, flowers=flowers, max_number=total)


def create_fl_from_str(string: str) -> Union[Flower, None]:
    flower_match = re.match(flowers_re, string)
    try:
        species = flower_match.group(1)
        size = flower_match.group(2)
    except AttributeError:
        return None
    return Flower(species=species, size=size)


def adjust_size(bouquet: Bouquet, difference: int) -> Bouquet:
    """Checks for flowers exceeding max number for given bq design and tries to remove"""
    while difference > 0:
        for f in bouquet.storage:
            total_count = len(bouquet.storage[f])
            updated_count = total_count
            if f in bouquet.flowers:
                updated_count = total_count - bouquet.flowers[f]
            if updated_count > 0:
                bouquet.storage[f] = bouquet.storage[f][1:]
                difference -= 1
            if difference == 0:
                break
    return bouquet
