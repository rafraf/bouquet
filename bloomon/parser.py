from bloomon.producer import BouquetProducer
from bloomon.utils import create_fl_from_str, create_bq_from_str


class InputStreamer:
    producer = None
    flowers_stream = False

    def __init__(self, bq_producer: BouquetProducer):
        self.producer = bq_producer

    def process(self, line: str):
        if line == '\n':
            self.flowers_stream = True
            return
        if not self.flowers_stream:
            bq = create_bq_from_str(line)
            self.producer.add_design(bq)
        else:
            flower = create_fl_from_str(line)
            bouquet = self.producer.compile_bouquet(flower=flower)

            if bouquet:
                print(bouquet)
