import copy
from typing import Union, List

from bloomon.bouquet import Bouquet, Flower
from bloomon.utils import adjust_size


class BouquetProducer:
    bouquet_designs = []

    def __init__(self):
        self.bouquet_designs = []

    def add_design(self, bouquet_design: Bouquet):
        """Add bouquet design to list"""
        self.bouquet_designs.append(bouquet_design)

    def compile_bouquet(self, flower: Flower) -> Union[Bouquet, None]:
        """Add given flower to bouquet designs and check if one of them have enough flowers according to specs"""
        complete_bq = None
        for bq in self.get_designs(flower.size):
            bq.add(flower)
            if bq.complete():
                complete_bq = bq
        if complete_bq:
            complete_bq = self.issue_bouquet(complete_bq)
        return complete_bq

    def get_designs(self, size: str) -> List[Bouquet]:
        """Get bouquet designs of given size"""
        return [bq for bq in self.bouquet_designs if bq.size == size]

    def remove_fl_used_in_bouquet(self, bouquet: Bouquet):
        """Remove flowers used in given bouquet from all bouquet designs"""
        for design in self.get_designs(bouquet.size):
            for f in bouquet.storage:
                if f in design.storage:
                    design.storage[f] = design.storage[f][len(bouquet.storage[f]):]

    def issue_bouquet(self, bouquet: Bouquet) -> Bouquet:
        """Removes excess flowers from the bouquet, and removes flowers used in bq from the designs"""
        new_bq = copy.deepcopy(bouquet)
        flower_number = sum(len(new_bq.storage[f]) for f in new_bq.storage)
        difference = flower_number - new_bq.max_number
        if difference > 0:
            new_bq = adjust_size(new_bq, difference)
        self.remove_fl_used_in_bouquet(new_bq)
        return new_bq
